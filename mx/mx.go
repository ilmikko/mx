package mx

import (
	"bufio"
	"bytes"
	"os"
	"strings"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx/command"
	"gitlab.com/ilmikko/mx/mx/parser"
	"gitlab.com/ilmikko/mx/mx/runes"
	"gitlab.com/ilmikko/mx/types/rule"
)

type MX struct {
	CFG *config.Config

	Cc *command.Command
}

func (m *MX) ProcessActions(actions []string, w runes.Writer) error {
	str := ""
	for i := range actions {
		m := rule.MACRO.String()
		str += m + actions[i] + m
	}
	if err := m.ProcessReader(strings.NewReader(str), w); err != nil {
		return err
	}
	return nil
}

func (m *MX) ProcessBytes(b []byte) ([]byte, error) {
	w := &bytes.Buffer{}

	if err := m.ProcessReader(bytes.NewReader(b), w); err != nil {
		return nil, err
	}

	return w.Bytes(), nil
}

func (m *MX) ProcessFile(path string, w runes.Writer) error {
	p := parser.New(m.CFG)
	p.W = w

	if err := m.Cc.NewRun("INCLUDE", p, path); err != nil {
		return err
	}

	return nil
}

func (m *MX) ProcessReader(r runes.Reader, w runes.Writer) error {
	p := parser.New(m.CFG)
	p.R = r
	p.W = w

	if err := m.Process(p); err != nil {
		return err
	}

	return nil
}

func (m *MX) ProcessStdin(w runes.Writer) error {
	// Do not wait for STDIN if there's nothing there.
	if stat, _ := os.Stdin.Stat(); (stat.Mode() & os.ModeCharDevice) == 0 {
		i := bufio.NewReader(os.Stdin)
		if err := m.ProcessReader(i, w); err != nil {
			return err
		}
	}

	return nil
}

func (m *MX) ProcessString(s string) (string, error) {
	w := &bytes.Buffer{}

	if err := m.ProcessReader(strings.NewReader(s), w); err != nil {
		return "", err
	}

	return w.String(), nil
}

func (m *MX) Process(p *parser.Parser) error {
	if err := m.Cc.NewRun("EVAL", p, ""); err != nil {
		return err
	}

	return nil
}

func New(cfg *config.Config) (*MX, error) {
	m := &MX{}

	m.CFG = cfg

	m.Cc = command.New(cfg)

	return m, nil
}
