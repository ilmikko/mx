package mx

import (
	"testing"

	"gitlab.com/ilmikko/mx/config"
)

func NewTesting(t *testing.T, cfg *config.Config) *MX {
	n, err := New(cfg)
	if err != nil {
		t.Fatalf("mx.New: %v", err)
	}

	return n
}
