package mx_test

import (
	"bytes"
	"fmt"
	"io"
	"strings"
	"testing"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx"
	"gitlab.com/ilmikko/mx/mx/parser"
)

func TestUnknown(t *testing.T) {
	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	p := parser.New(cfg)
	p.R = strings.NewReader("Data with an `UNKNOWN COMMAND`!")

	err := n.Process(p)

	switch err {
	case nil:
		t.Errorf("Expected error parser did not return one!")
	case io.EOF:
		t.Errorf("Expected error but got EOF instead!")
	}

	got := err.Error()
	want := "Unknown command: UNKNOWN"
	if got != want {
		t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
	}
}

func TestEscapeRunes(t *testing.T) {
	testCases := []struct {
		data    []byte
		want    string
		wantErr error
	}{
		{
			data: []byte("You can even escape the escape character `ECHO \\\\`. Original \\ stays the same."),
			want: "You can even escape the escape character \\. Original \\ stays the same.",
		},
		{
			data: []byte("This quote `ECHO \\`` is escaped by the presence of an escape rune."),
			want: "This quote ` is escaped by the presence of an escape rune.",
		},
		{
			data: []byte("These quotes `ECHO \\`\\`` are escaped by their presence."),
			want: "These quotes `` are escaped by their presence.",
		},
		{
			data: []byte("What happens if I leave a quote `\\` unescaped?"),
			want: "What happens if I leave a quote ",
		},
		{
			data: []byte("These `ECHO \\`quotes\\`` are escaped by their presence."),
			want: "These `quotes` are escaped by their presence.",
		},
		{
			data: []byte("This ECHO `ECHO \\`ECHO hello world\\`` is not executed."),
			want: "This ECHO `ECHO hello world` is not executed.",
		},
		{
			data: []byte("Non-special n becomes `ECHO Very Special\\r\\n` but not outside\\r\\n!"),
			want: "Non-special n becomes Very Special\r\n but not outside\\r\\n!",
		},
		{
			data: []byte("Do you like `ECHO Tabs\\t or Spaces\\s?` not \\t or \\t!"),
			want: "Do you like Tabs\t or Spaces ? not \\t or \\t!",
		},
		{
			data:    []byte("Escaped space: `ECHO\\ COMMAND` returns an error."),
			wantErr: fmt.Errorf("Unknown command: ECHO COMMAND"),
			want:    "Escaped space: ",
		},
	}

	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	for _, tc := range testCases {
		p := parser.New(cfg)

		p.R = bytes.NewReader(tc.data)

		err := n.Process(p)
		if err != nil {
			if err != io.EOF {
				if tc.wantErr != nil {
					if want, got := tc.wantErr.Error(), err.Error(); want != got {
						t.Errorf("Error not as expected:\n got: %q\nwant: %q", got, want)
					}
				} else {
					t.Errorf("p.Next unexpected error: %v", err)
				}
			}
		} else if tc.wantErr != nil {
			t.Errorf("Expected an error but got nil!")
		}

		got := fmt.Sprintf("%s", p.W)
		want := tc.want
		if got != want {
			t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
		}
	}
}

func TestHappensToHaveNoMacro(t *testing.T) {
	data := []byte(
		`
			##### Excerpt from "I have all the ASCII characters and I must scream" #####
			This is a string with no macros (but some other characters).
			It should remain unchanged!
			Spectator: 'But what if it doesn't?'
			That means there is a \"regression\".
			I am sure you know what that means; it is 1+1=2 after all.
			And if not, I - and don't get me wrong here - think you should consult a dictionary.
			[whispers] Oxford or Harvard...? Either/or, or either\or?
			I'm not 100%% sure what you mean.
			~ END ~
		`,
	)

	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	p := parser.New(cfg)
	p.R = bytes.NewReader(data)

	if err := n.Process(p); err != nil {
		if err != io.EOF {
			t.Errorf("n.Process: %v", err)
		}
	}

	got := fmt.Sprintf("%s", p.W)
	want := string(data)
	if got != want {
		t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
	}
}

func TestWhyWouldUnicodeBreak(t *testing.T) {
	data := []byte(
		`
			Mood when program works on the first try: 🤔
			Mood when it works locally but not in prod: 😐
			Mood after 2 hours of debugging: 🙂
			Fact: Developers from 🇫🇮 earn more sauna time than your average developer.
		`,
	)

	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	p := parser.New(cfg)
	p.R = bytes.NewReader(data)

	if err := n.Process(p); err != nil {
		if err != io.EOF {
			t.Errorf("n.Process: %v", err)
		}
	}

	got := fmt.Sprintf("%s", p.W)
	want := string(data)
	if got != want {
		t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
	}
}
