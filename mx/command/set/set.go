package set

import (
	"bytes"

	"gitlab.com/ilmikko/mx/mx/parser"
)

type Set struct{}

func (c *Set) Run(p *parser.Parser, a string) error {
	b := &bytes.Buffer{}
	if err := p.Stream.Copy(p.R, b); err != nil {
		return err
	}
	p.Variables.Set(a, b)
	return nil
}

func New() *Set {
	return &Set{}
}
