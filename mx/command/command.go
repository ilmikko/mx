package command

import (
	"bytes"
	"fmt"
	"strings"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx/parser"
	"gitlab.com/ilmikko/mx/types/command"
)

type Cmd struct {
	Arg      string
	runnable Runnable
}

func (c *Cmd) Run(p *parser.Parser) error {
	if err := c.runnable.Run(p, c.Arg); err != nil {
		return err
	}
	return nil
}

type Command struct {
	CFG       *config.Config
	Runnables Runnables
}

func (cc *Command) Choose(name string) (Runnable, error) {
	name = strings.ToUpper(name)

	cmd, ok := cc.CFG.Commands[name]
	if !ok {
		return nil, fmt.Errorf("Unknown command: %s", name)
	}

	r, ok := cc.Runnables[cmd]
	if !ok {
		return nil, fmt.Errorf("BUG: No runnable for command: %q %v", name, cmd)
	}
	return r, nil
}

func (cc *Command) NewRun(cmd string, p *parser.Parser, a string) error {
	c, err := cc.New(cmd, a)
	if err != nil {
		return err
	}
	if err := c.Run(p); err != nil {
		return err
	}
	return nil
}

func (cc *Command) New(name string, arg string) (*Cmd, error) {
	c := &Cmd{}

	c.Arg = arg

	r, err := cc.Choose(name)
	if err != nil {
		return nil, err
	}
	c.runnable = r

	return c, nil
}

// RunCloned creates a temporary clone for a Parser context for the commands.
func (cc *Command) RunCloned(p *parser.Parser, cmds ...*Cmd) error {
	np, err := p.Clone()
	if err != nil {
		return err
	}

	if err := cc.RunDetached(np, p, cmds...); err != nil {
		return err
	}
	return nil
}

// RunDetached uses a different Parser for the commands than the output.
func (cc *Command) RunDetached(p *parser.Parser, op *parser.Parser, cmds ...*Cmd) error {
	if err := cc.Run(p, cmds...); err != nil {
		return err
	}

	p.Swap()
	if err := p.Stream.Copy(p.R, op.W); err != nil {
		return err
	}
	return nil
}

func (cc *Command) Run(p *parser.Parser, cmds ...*Cmd) error {
	// Clone a new parser for the pipeline
	np, err := p.Clone()
	if err != nil {
		return err
	}

	for _, c := range cmds {
		if err := c.Run(np); err != nil {
			return err
		}
		np.Swap()
		np.W = &bytes.Buffer{}
	}

	// After the last swap, the pipeline's written data is in np.R.
	if err := p.Stream.Copy(np.R, p.W); err != nil {
		return err
	}
	return nil
}

func New(cfg *config.Config) *Command {
	cc := &Command{}

	cc.CFG = cfg

	cc.Runnables = NewRunnables()
	// TODO: Is there a way to not have these special commands w/ self reference?
	cc.Runnables[command.EVAL] = &Eval{Cc: cc}
	cc.Runnables[command.INCLUDE] = &Include{Cc: cc}

	return cc
}
