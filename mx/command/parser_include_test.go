package command_test

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx"
	"gitlab.com/ilmikko/mx/mx/parser"
)

func TestCmdInclude(t *testing.T) {
	tmpfile, err := ioutil.TempFile("", "example")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())

	tmpfile.Write([]byte("include a file"))
	tmpfile.Close()

	execfile, err := ioutil.TempFile("", "example")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(execfile.Name())

	execfile.Write([]byte(
		fmt.Sprintf("also include `ECHO new macros` or even `INCLUDE %s`", tmpfile.Name()),
	))
	execfile.Close()

	testCases := []struct {
		data []byte
		want string
	}{
		{
			data: []byte(
				fmt.Sprintf("Include will `INCLUDE %s`.", tmpfile.Name()),
			),
			want: "Include will include a file.",
		},
		{
			data: []byte(
				fmt.Sprintf("Includes can `INCLUDE %s`!", execfile.Name()),
			),
			want: "Includes can also include new macros or even include a file!",
		},
	}

	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	for _, tc := range testCases {
		p := parser.New(cfg)
		p.R = bytes.NewReader(tc.data)

		if err := n.Process(p); err != nil {
			if err != io.EOF {
				t.Errorf("p.Next: %v", err)
			}
		}

		got := fmt.Sprintf("%s", p.W)
		want := tc.want
		if got != want {
			t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
		}
	}
}
