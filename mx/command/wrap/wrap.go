package wrap

import (
	"strings"

	"gitlab.com/ilmikko/mx/mx/parser"
)

type Wrap struct{}

func (c *Wrap) Run(p *parser.Parser, a string) error {
	f := strings.Split(a, " ")

	if len(f) > 0 {
		if err := p.Stream.Copy(strings.NewReader(f[0]), p.W); err != nil {
			return err
		}
	}
	if err := p.Stream.Copy(p.R, p.W); err != nil {
		return err
	}
	if len(f) > 1 {
		if err := p.Stream.Copy(strings.NewReader(f[1]), p.W); err != nil {
			return err
		}
	}
	return nil
}

func New() *Wrap {
	return &Wrap{}
}

type WrapLeft struct{}

func (c *WrapLeft) Run(p *parser.Parser, a string) error {
	if err := p.Stream.Copy(strings.NewReader(a), p.W); err != nil {
		return err
	}
	if err := p.Stream.Copy(p.R, p.W); err != nil {
		return err
	}
	return nil
}

func NewLeft() *WrapLeft {
	return &WrapLeft{}
}

type WrapRight struct{}

func (c *WrapRight) Run(p *parser.Parser, a string) error {
	if err := p.Stream.Copy(p.R, p.W); err != nil {
		return err
	}
	if err := p.Stream.Copy(strings.NewReader(a), p.W); err != nil {
		return err
	}
	return nil
}

func NewRight() *WrapRight {
	return &WrapRight{}
}
