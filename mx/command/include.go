package command

import (
	"strings"

	"gitlab.com/ilmikko/mx/mx/parser"
)

type Include struct {
	Cc *Command
}

func (c *Include) Run(p *parser.Parser, a string) error {
	a = p.Fc.RealPath(a)

	exts := strings.Split(p.Fc.FileName(a), ".")
	for i := 1; i < len(exts); i++ {
		dialect, err := c.Cc.New("DIALECT", exts[i])
		if err != nil {
			return err
		}
		if err := c.Cc.Run(p, dialect); err != nil {
			return err
		}
	}

	read, err := c.Cc.New("READ", a)
	if err != nil {
		return err
	}

	eval, err := c.Cc.New("EVAL", "")
	if err != nil {
		return err
	}

	np, err := p.Clone()
	if err != nil {
		return err
	}
	np.Fc.ChFile(a)

	if err := c.Cc.RunDetached(np, p, read, eval); err != nil {
		return err
	}
	return nil
}
