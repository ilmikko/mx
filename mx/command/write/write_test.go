package write_test

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx"
	"gitlab.com/ilmikko/mx/mx/parser"
)

func TestCmdWrite(t *testing.T) {
	file, err := ioutil.TempFile("", "example")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	testCases := []struct {
		data     []byte
		want     string
		wantFile string
	}{
		{
			data: []byte(
				fmt.Sprintf("Write this `ECHO This is file1|WRITE %s`.", file.Name()),
			),
			want:     "Write this .",
			wantFile: "This is file1",
		},
		{
			data: []byte(
				fmt.Sprintf("And this `ECHO This is file2|WRITE %s|ECHO but not this`.", file.Name()),
			),
			want:     "And this but not this.",
			wantFile: "This is file2",
		},
	}

	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	for _, tc := range testCases {
		p := parser.New(cfg)
		p.R = bytes.NewReader(tc.data)

		if err := n.Process(p); err != nil {
			if err != io.EOF {
				t.Errorf("p.Next: %v", err)
			}
		}

		{
			got := fmt.Sprintf("%s", p.W)
			want := tc.want
			if got != want {
				t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
			}
		}

		{
			data, err := ioutil.ReadFile(file.Name())
			if err != nil {
				t.Errorf("Failed to read file %q: %v", file.Name(), err)
			}
			got := string(data)
			want := tc.wantFile
			if got != want {
				t.Errorf("File contents not as expected: \n got: %q\nwant: %q", got, want)
			}
		}
	}
}
