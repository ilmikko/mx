package write

import (
	"bufio"
	"log"
	"os"

	"gitlab.com/ilmikko/mx/mx/parser"
)

type Write struct{}

func (c *Write) Run(p *parser.Parser, a string) error {
	// Use default write buffer.
	if a == "" {
		return nil
	}

	f, err := os.Create(a)
	if err != nil {
		return err
	}
	w := bufio.NewWriter(f)
	p.W = w
	if err := p.Stream.Copy(p.R, p.W); err != nil {
		log.Printf("E: %v", err)
		return err
	}
	if err := w.Flush(); err != nil {
		return err
	}
	return nil
}

func New() *Write {
	return &Write{}
}
