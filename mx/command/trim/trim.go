package trim

import (
	"bytes"
	"io"
	"strings"

	"gitlab.com/ilmikko/mx/mx/parser"
)

func trimLeft(p *parser.Parser, a string) error {
	discard := &bytes.Buffer{}
	for _, tr := range []rune(a) {
		r, err := p.Stream.ReadRune(p.R)
		if err != nil {
			return err
		}
		discard.WriteRune(r)
		if r != tr {
			// Oops, the stream that we have read did not match after all.
			// Let's write it back into the buffer.
			if err := p.Stream.Copy(discard, p.W); err != nil {
				return err
			}
			break
		}
	}
	return nil
}

func trimRight(p *parser.Parser, a string) error {
	discard := &bytes.Buffer{}
	runes := append([]rune(a), 0)
	for {
		for i, tr := range runes {
			r, err := p.Stream.ReadRune(p.R)
			if err != nil {
				if err == io.EOF {
					if i != len(runes)-1 {
						if err := p.Stream.Copy(discard, p.W); err != nil {
							return err
						}
					}
					return nil
				}
				return err
			}
			discard.WriteRune(r)
			if r != tr {
				break
			}
		}
		if err := p.Stream.Copy(discard, p.W); err != nil {
			return err
		}
		discard.Reset()
	}
}

type Trim struct{}

func (c *Trim) Run(p *parser.Parser, a string) error {
	f := strings.Split(a, " ")

	if len(f) > 0 {
		if err := trimLeft(p, f[0]); err != nil {
			return err
		}
	}
	if len(f) > 1 {
		if err := trimRight(p, f[1]); err != nil {
			return err
		}
		return nil
	}
	if err := p.Stream.Copy(p.R, p.W); err != nil {
		return err
	}
	return nil
}

func New() *Trim {
	return &Trim{}
}

type TrimLeft struct{}

func (c *TrimLeft) Run(p *parser.Parser, a string) error {
	if err := trimLeft(p, a); err != nil {
		return err
	}
	if err := p.Stream.Copy(p.R, p.W); err != nil {
		return err
	}
	return nil
}

func NewLeft() *TrimLeft {
	return &TrimLeft{}
}

type TrimRight struct{}

func (c *TrimRight) Run(p *parser.Parser, a string) error {
	if err := trimRight(p, a); err != nil {
		return err
	}
	return nil
}

func NewRight() *TrimRight {
	return &TrimRight{}
}
