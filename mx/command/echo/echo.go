package echo

import (
	"strings"

	"gitlab.com/ilmikko/mx/mx/parser"
)

type Echo struct{}

func (c *Echo) Run(p *parser.Parser, a string) error {
	if err := p.Stream.Copy(p.R, p.W); err != nil {
		return err
	}
	if err := p.Stream.Copy(strings.NewReader(a), p.W); err != nil {
		return err
	}
	return nil
}

func New() *Echo {
	return &Echo{}
}
