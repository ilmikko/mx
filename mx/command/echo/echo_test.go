package echo_test

import (
	"bytes"
	"fmt"
	"io"
	"testing"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx"
	"gitlab.com/ilmikko/mx/mx/parser"
)

func TestParseEcho(t *testing.T) {
	testCases := []struct {
		data []byte
		want string
	}{
		{
			data: []byte("Echo will simply `ECHO replace text`."),
			want: "Echo will simply replace text.",
		},
		{
			data: []byte("`ECHO marco``ECHO polo`!"),
			want: "marcopolo!",
		},
		{
			data: []byte("Unless you use it wrong `ECHO`."),
			want: "Unless you use it wrong .",
		},
		{
			data: []byte("Even Unicode works! `ECHO ✓`."),
			want: "Even Unicode works! ✓.",
		},
		{
			data: []byte("You can even `ECHO \\`ECHO again\\`!`!"),
			want: "You can even `ECHO again`!!",
		},
		{
			data: []byte("`ECHO Echo some newlines\n!`"),
			want: "Echo some newlines\n!",
		},
		{
			data: []byte("`ECHO Echo some newlines\\n!`"),
			want: "Echo some newlines\n!",
		},
		{
			data: []byte("Piping into echo: `ECHO one|ECHO two`."),
			want: "Piping into echo: onetwo.",
		},
	}

	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	for _, tc := range testCases {
		p := parser.New(cfg)
		p.R = bytes.NewReader(tc.data)

		if err := n.Process(p); err != nil {
			if err != io.EOF {
				t.Errorf("p.Next: %v", err)
			}
		}

		got := fmt.Sprintf("%s", p.W)
		want := tc.want
		if got != want {
			t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
		}
	}
}
