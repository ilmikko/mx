package dialect

import (
	"gitlab.com/ilmikko/mx/mx/parser"
)

type Dialect struct{}

func (c *Dialect) Run(p *parser.Parser, a string) error {
	return nil
}

func New() *Dialect {
	return &Dialect{}
}
