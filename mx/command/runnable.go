package command

import (
	"gitlab.com/ilmikko/mx/mx/command/dialect"
	"gitlab.com/ilmikko/mx/mx/command/echo"
	"gitlab.com/ilmikko/mx/mx/command/get"
	"gitlab.com/ilmikko/mx/mx/command/override"
	"gitlab.com/ilmikko/mx/mx/command/pwd"
	"gitlab.com/ilmikko/mx/mx/command/read"
	"gitlab.com/ilmikko/mx/mx/command/remove"
	"gitlab.com/ilmikko/mx/mx/command/set"
	"gitlab.com/ilmikko/mx/mx/command/trim"
	"gitlab.com/ilmikko/mx/mx/command/wrap"
	"gitlab.com/ilmikko/mx/mx/command/write"
	"gitlab.com/ilmikko/mx/mx/parser"
	"gitlab.com/ilmikko/mx/types/command"
)

type Runnable interface {
	Run(p *parser.Parser, a string) error
}

type Runnables map[command.Command]Runnable

func NewRunnables() Runnables {
	r := Runnables{}

	r[command.DIALECT] = dialect.New()
	r[command.OVERRIDE] = override.New()

	r[command.GET] = get.New()
	r[command.SET] = set.New()

	r[command.READ] = read.New()
	r[command.WRITE] = write.New()

	r[command.ECHO] = echo.New()
	r[command.PWD] = pwd.New()

	r[command.REMOVE] = remove.New()

	r[command.WRAP] = wrap.New()
	r[command.WRAP_LEFT] = wrap.NewLeft()
	r[command.WRAP_RIGHT] = wrap.NewRight()

	r[command.TRIM] = trim.New()
	r[command.TRIM_LEFT] = trim.NewLeft()
	r[command.TRIM_RIGHT] = trim.NewRight()

	return r
}
