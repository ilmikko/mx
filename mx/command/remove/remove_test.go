package remove_test

import (
	"bytes"
	"fmt"
	"io"
	"testing"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx"
	"gitlab.com/ilmikko/mx/mx/command"
	"gitlab.com/ilmikko/mx/mx/parser"
)

func TestRemove(t *testing.T) {
	testCases := []struct {
		data []byte
		want string
		args string
	}{
		{
			data: []byte("Remove some characters."),
			args: "aeo",
			want: "Rmv sm chrctrs.",
		},
		{
			data: []byte("Remove some characters."),
			args: " ",
			want: "Removesomecharacters.",
		},
		{
			data: []byte("Remove nothing."),
			args: "",
			want: "Remove nothing.",
		},
		{
			data: []byte("Remove the newlines!\n\n\n"),
			args: "\n",
			want: "Remove the newlines!",
		},
	}

	cfg := config.NewTesting(t)

	for _, tc := range testCases {
		p := parser.New(cfg)
		p.R = bytes.NewReader(tc.data)

		if err := command.New(cfg).NewRun("REMOVE", p, tc.args); err != nil {
			if err != io.EOF {
				t.Errorf("c.Remove: %v", err)
			}
		}

		got := fmt.Sprintf("%s", p.W)
		want := tc.want
		if got != want {
			t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
		}
	}
}

func TestParseRemove(t *testing.T) {
	testCases := []struct {
		data []byte
		want string
	}{
		{
			data: []byte("Remove will remove characters. `ECHO Remove some characters|REMOVE aeo`."),
			want: "Remove will remove characters. Rmv sm chrctrs.",
		},
		{
			data: []byte("Remove will remove characters. `ECHO Remove some characters|REMOVE  `."),
			want: "Remove will remove characters. Removesomecharacters.",
		},
		{
			data: []byte("Remove will remove characters. `ECHO Remove nothing|REMOVE`."),
			want: "Remove will remove characters. Remove nothing.",
		},
		{
			data: []byte("Remove `ECHO Remove the newlines!\n\n\n|REMOVE \n` this!"),
			want: "Remove Remove the newlines! this!",
		},
		{
			data: []byte("Remove `ECHO Remove the newlines!\\n\\n\\n|REMOVE \\n` this!"),
			want: "Remove Remove the newlines! this!",
		},
	}

	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	for _, tc := range testCases {
		p := parser.New(cfg)
		p.R = bytes.NewReader(tc.data)

		if err := n.Process(p); err != nil {
			if err != io.EOF {
				t.Errorf("p.Next: %v", err)
			}
		}

		got := fmt.Sprintf("%s", p.W)
		want := tc.want
		if got != want {
			t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
		}
	}
}
