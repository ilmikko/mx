package remove

import (
	"io"
	"strings"

	"gitlab.com/ilmikko/mx/mx/parser"
)

type Remove struct{}

func (c *Remove) Run(p *parser.Parser, a string) error {
	r, w := p.R, p.W
	set := map[rune]struct{}{}

	ar := strings.NewReader(a)
	for {
		rn, err := p.Stream.ReadRune(ar)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		set[rn] = struct{}{}
	}

	for {
		rn, err := p.Stream.ReadRune(r)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		if _, ok := set[rn]; ok {
			continue
		}

		if err := p.Stream.WriteRune(w, rn); err != nil {
			return err
		}
	}
	return nil
}

func New() *Remove {
	return &Remove{}
}
