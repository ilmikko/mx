package override

import (
	"io"

	"gitlab.com/ilmikko/mx/mx/parser"
)

type Override struct{}

func (c *Override) Run(p *parser.Parser, a string) error {
	or := []rune(a)[0]
	rule, err := p.CFG.GetRule(or)
	if err != nil {
		return err
	}

	delete(p.CFG.Rules, rule)
	nr, err := p.Stream.ReadRune(p.R)
	if err != nil {
		if err == io.EOF {
			return nil
		}
		return err
	}
	p.CFG.Rules[rule] = nr
	return nil
}

func New() *Override {
	return &Override{}
}
