package pwd

import (
	"gitlab.com/ilmikko/mx/mx/parser"
)

type Pwd struct{}

func (c *Pwd) Run(p *parser.Parser, a string) error {
	for _, r := range []rune(p.Fc.Dir) {
		p.W.WriteRune(r)
	}
	return nil
}

func New() *Pwd {
	return &Pwd{}
}
