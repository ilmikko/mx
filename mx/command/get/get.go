package get

import (
	"gitlab.com/ilmikko/mx/mx/parser"
)

type Get struct{}

func (c *Get) Run(p *parser.Parser, a string) error {
	b := p.Variables.Get(a)
	if err := p.Stream.Copy(b, p.W); err != nil {
		return err
	}
	return nil
}

func New() *Get {
	return &Get{}
}
