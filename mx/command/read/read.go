package read

import (
	"bufio"
	"os"

	"gitlab.com/ilmikko/mx/mx/parser"
)

type Read struct{}

func (c *Read) Run(p *parser.Parser, a string) error {
	f, err := os.Open(
		p.Fc.RealPath(a),
	)
	if err != nil {
		return err
	}
	defer f.Close()

	// TODO: Could we do this without buffering? We could return the reader and be lazy about it.
	if err := p.Stream.Copy(bufio.NewReader(f), p.W); err != nil {
		return err
	}
	return nil
}

func New() *Read {
	return &Read{}
}
