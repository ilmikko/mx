package read_test

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx"
	"gitlab.com/ilmikko/mx/mx/parser"
)

func TestCmdRead(t *testing.T) {
	tmpfile, err := ioutil.TempFile("", "example")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpfile.Name())

	tmpfile.WriteString("read a file")
	tmpfile.Close()

	execfile, err := ioutil.TempFile("", "example")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(execfile.Name())

	execfile.WriteString("Macros `ECHO macros` are not executed.")
	execfile.Close()

	testCases := []struct {
		data []byte
		want string
	}{
		{
			data: []byte(
				fmt.Sprintf("Read will `READ %s`.", tmpfile.Name()),
			),
			want: "Read will read a file.",
		},
		{
			data: []byte(
				fmt.Sprintf("Note that `READ %s`", execfile.Name()),
			),
			want: "Note that Macros `ECHO macros` are not executed.",
		},
	}

	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	for _, tc := range testCases {
		p := parser.New(cfg)
		p.R = bytes.NewReader(tc.data)

		if err := n.Process(p); err != nil {
			if err != io.EOF {
				t.Errorf("p.Next: %v", err)
			}
		}

		got := fmt.Sprintf("%s", p.W)
		want := tc.want
		if got != want {
			t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
		}
	}
}
