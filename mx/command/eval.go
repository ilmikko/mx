package command

import (
	"io"

	"gitlab.com/ilmikko/mx/mx/parser"
	"gitlab.com/ilmikko/mx/types/rule"
)

type Eval struct {
	Cc *Command
}

func (c *Eval) Run(p *parser.Parser, a string) error {
	if a != "" {
		// EVAL x is a shorthand for ECHO `x`|EVAL.
		// TODO: This isn't 100% correct - it should be our current RULE_MACRO, not default.
		m := rule.MACRO.String()
		echo, err := c.Cc.New("ECHO", m+a+m)
		if err != nil {
			return err
		}
		eval, err := c.Cc.New("EVAL", "")
		if err != nil {
			return err
		}
		if err := c.Cc.RunCloned(p, echo, eval); err != nil {
			return err
		}
	}
	for {
		macro, err := p.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		pl, err := p.PipeLine(macro)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}

		cmds := []*Cmd{}
		for _, a := range pl {
			name, arg, err := p.Command(a)
			if err != nil {
				return err
			}

			cmd, err := c.Cc.New(name, arg)
			if err != nil {
				return err
			}
			cmds = append(cmds, cmd)
		}

		if err := c.Cc.Run(p, cmds...); err != nil {
			return err
		}
	}
	return nil
}
