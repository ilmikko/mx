package command_test

import (
	"bytes"
	"fmt"
	"testing"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx"
	"gitlab.com/ilmikko/mx/mx/parser"
)

func TestParseEval(t *testing.T) {
	testCases := []struct {
		data    []byte
		want    string
		wantErr error
	}{
		{
			data: []byte("Evaluate this: `ECHO \\`ECHO hello world\\`!|EVAL`"),
			want: "Evaluate this: hello world!",
		},
		{
			data:    []byte("Evaluation error: `ECHO \\`UNDEFINED COMMAND\\`!|EVAL`"),
			want:    "Evaluation error: ",
			wantErr: fmt.Errorf("Unknown command: UNDEFINED"),
		},
		{
			data: []byte("Empty eval: `EVAL`"),
			want: "Empty eval: ",
		},
	}

	cfg := config.NewTesting(t)
	n := mx.NewTesting(t, cfg)

	for _, tc := range testCases {
		p := parser.New(cfg)
		p.R = bytes.NewReader(tc.data)

		err := n.Process(p)
		if tc.wantErr != nil {
			if err == nil {
				t.Errorf("Expected an error %q but was nil!", tc.wantErr.Error())
			} else {
				got := err.Error()
				want := tc.wantErr.Error()
				if got != want {
					t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
				}
			}
		} else {
			if err != nil {
				t.Errorf("p.Next: %v", err)
			}
		}

		got := fmt.Sprintf("%s", p.W)
		want := tc.want
		if got != want {
			t.Errorf("Output not as expected:\n got: %q\nwant: %q", got, want)
		}
	}
}
