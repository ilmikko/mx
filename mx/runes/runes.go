package runes

import (
	"bytes"
	"fmt"
	"io"

	"gitlab.com/ilmikko/mx/config"
)

const (
	EOF = '\x00'
)

type Reader interface {
	ReadRune() (rune, int, error)
	UnreadRune() error
}

type Writer interface {
	WriteRune(rune) (int, error)
}

type ReadWriter interface {
	Reader
	Writer
}

type Runes struct {
	CFG *config.Config
}

func (rns *Runes) Escape(rn rune) (rune, error) {
	if rns.CFG.IsSpecialRune(rn) {
		return rn, nil
	}

	switch rn {
	case 'a': // Alert
		return '\a', nil
	case 'b': // Backspace
		return '\b', nil
	case 'f': // Form Feed
		return '\f', nil
	case 'n': // Newline
		return '\n', nil
	case 'r': // Carriage Return
		return '\r', nil
	case 's': // Space
		return ' ', nil
	case 't': // Tab
		return '\t', nil
	case 'v': // Vertical Tab
		return '\v', nil
	default:
		return '\x00', fmt.Errorf("Unknown escape sequence: \\%s", string(rn))
	}
}

func New(cfg *config.Config) *Runes {
	rns := &Runes{}

	rns.CFG = cfg

	return rns
}

func MoveSingle(r Reader, w Writer) error {
	rn, _, err := r.ReadRune()
	if err != nil {
		return err
	}
	if _, err := w.WriteRune(rn); err != nil {
		return err
	}

	return nil
}

func Move(r Reader, w Writer) error {
	for {
		if err := MoveSingle(r, w); err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
	}
	return nil
}

func Split(r Reader) (ReadWriter, ReadWriter, error) {
	w := &bytes.Buffer{}
	c := &bytes.Buffer{}

	if err := Tee(r, w, c); err != nil {
		return nil, nil, err
	}

	return w, c, nil
}

func TeeSingle(r Reader, w Writer, t Writer) error {
	rn, _, err := r.ReadRune()
	if err != nil {
		return err
	}
	if _, err := w.WriteRune(rn); err != nil {
		return err
	}
	if _, err := t.WriteRune(rn); err != nil {
		return err
	}

	return nil
}

func Tee(r Reader, w Writer, t Writer) error {
	for {
		if err := TeeSingle(r, w, t); err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
	}
	return nil
}
