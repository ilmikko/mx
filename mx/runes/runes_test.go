package runes_test

import (
	"fmt"
	"testing"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx/runes"
)

func TestEscape(t *testing.T) {
	testCases := []struct {
		escape  rune
		want    rune
		wantErr error
	}{
		{escape: 'a', want: '\a'},
		{escape: 'b', want: '\b'},
		{escape: 'f', want: '\f'},
		{escape: 'n', want: '\n'},
		{escape: 'r', want: '\r'},
		{escape: 't', want: '\t'},
		{escape: 'v', want: '\v'},

		// \s is a regular space.
		{escape: 's', want: ' '},
		// Unknown escape sequences return an error.
		{
			escape:  '!',
			wantErr: fmt.Errorf("Unknown escape sequence: \\!"),
		},
	}

	r := runes.New(config.NewTesting(t))

	for _, tc := range testCases {
		got, err := r.Escape(tc.escape)
		if tc.wantErr != nil {
			got := err.Error()
			want := tc.wantErr.Error()
			if got != want {
				t.Errorf("Escape error not as expected:\n got: %q\nwant: %q", got, want)
			}
		} else {
			if err != nil {
				t.Errorf("r.Escape: %v", err)
			}
		}
		want := tc.want
		if got != want {
			t.Errorf("Escaped rune not as expected:\n got: %q\nwant: %q", got, want)
		}
	}
}
