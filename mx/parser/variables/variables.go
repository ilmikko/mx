package variables

import (
	"bytes"

	"gitlab.com/ilmikko/mx/mx/runes"
)

type Variables struct {
	data map[string]runes.ReadWriter
}

func (vv *Variables) Clone() *Variables {
	nv := New()

	for k := range vv.data {
		nv.data[k] = vv.Get(k)
	}

	return nv
}

func (vv *Variables) Get(k string) runes.ReadWriter {
	b := &bytes.Buffer{}

	v, ok := vv.data[k]
	if !ok {
		return b
	}

	c, v, _ := runes.Split(v)

	vv.data[k] = v
	return c
}

func (vv *Variables) Set(k string, v runes.ReadWriter) {
	vv.data[k] = v
}

func New() *Variables {
	v := &Variables{}

	v.data = map[string]runes.ReadWriter{}

	return v
}
