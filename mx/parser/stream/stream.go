package stream

import (
	"io"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx/runes"
	"gitlab.com/ilmikko/mx/types/rule"
)

type Stream struct {
	CFG *config.Config

	RunesRead int
	LinesRead int

	Runes *runes.Runes
}

// Same as ReadUntil, except does not respect escaping patterns.
// It simply copies runes over until a certain rune is found.
func (s *Stream) CopyUntil(r runes.Reader, w runes.Writer, rs ...rune) (rune, error) {
	for {
		rn, err := s.ReadRune(r)
		if err != nil {
			return runes.EOF, err
		}

		for _, trn := range rs {
			if rn == trn {
				return rn, nil
			}
		}

		if _, err := w.WriteRune(rn); err != nil {
			return runes.EOF, err
		}
	}
}

func (s *Stream) CopySingle(r runes.Reader, w runes.Writer) error {
	rn, err := s.ReadRune(r)
	if err != nil {
		return err
	}
	if _, err := w.WriteRune(rn); err != nil {
		return err
	}
	return nil
}

func (s *Stream) Copy(r runes.Reader, w runes.Writer) error {
	for {
		if _, err := s.CopyUntil(r, w); err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
	}
	return nil
}

func (s *Stream) ReadRuneEscaped(r runes.Reader) (rune, error) {
	rn, err := s.ReadRune(r)
	if err != nil {
		return runes.EOF, err
	}
	ern, err := s.Runes.Escape(rn)
	if err != nil {
		return runes.EOF, err
	}
	return ern, nil
}

func (s *Stream) ReadRuneSize(r runes.Reader) (rune, int, error) {
	rn, sz, err := r.ReadRune()
	if err != nil {
		return runes.EOF, 0, err
	}

	return rn, sz, nil
}

func (s *Stream) ReadRune(r runes.Reader) (rune, error) {
	rn, _, err := s.ReadRuneSize(r)
	if err != nil {
		return runes.EOF, err
	}

	// Useful debug information so we can display how many runes/lines we read
	// in case we end up with an error.
	s.RunesRead++
	if rn == '\n' {
		s.LinesRead++
	}

	return rn, nil
}

func (s *Stream) ReadUntil(r runes.Reader, w runes.Writer, rs ...rune) (rune, error) {
	for {
		rn, err := s.ReadRune(r)
		if err != nil {
			return runes.EOF, err
		}

		// Ignore any runes following an escape rune.
		if rn == s.CFG.Rules[rule.ESCAPE] {
			if _, err := w.WriteRune(rn); err != nil {
				return runes.EOF, err
			}
			if err := s.CopySingle(r, w); err != nil {
				return runes.EOF, err
			}
			continue
		}

		for _, trn := range rs {
			if rn == trn {
				return rn, nil
			}
		}

		if _, err := w.WriteRune(rn); err != nil {
			return runes.EOF, err
		}
	}
}

func (s *Stream) Unescape(r runes.Reader, w runes.Writer) error {
	for {
		rn, err := s.ReadRune(r)
		if err != nil {
			return err
		}

		if rn == s.CFG.Rules[rule.ESCAPE] {
			rn, err := s.ReadRuneEscaped(r)
			if err != nil {
				return err
			}
			w.WriteRune(rn)
			continue
		}

		w.WriteRune(rn)
	}
}

func (s *Stream) WriteRuneSize(w runes.Writer, rn rune) (int, error) {
	n, err := w.WriteRune(rn)
	if err != nil {
		return 0, err
	}
	return n, nil
}

func (s *Stream) WriteRune(w runes.Writer, rn rune) error {
	if _, err := s.WriteRuneSize(w, rn); err != nil {
		return err
	}
	return nil
}

func New(cfg *config.Config) *Stream {
	s := &Stream{}

	s.CFG = cfg
	s.Runes = runes.New(cfg)

	return s
}
