package stream_test

import (
	"bytes"
	"fmt"
	"io"
	"strings"
	"testing"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx/parser/stream"
)

func TestCopy(t *testing.T) {
	cfg := config.NewTesting(t)

	s := stream.New(cfg)
	r := &bytes.Buffer{}
	w := &bytes.Buffer{}

	r.WriteString("buffer with data")

	// Copy should not return EOF (as it would always return EOF)
	if err := s.Copy(r, w); err != nil {
		t.Errorf("s.Copy: %v", err)
	}
	{
		got := w.String()
		want := "buffer with data"
		if got != want {
			t.Errorf("Written buffer not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestCopySingle(t *testing.T) {
	cfg := config.NewTesting(t)

	s := stream.New(cfg)
	r := &bytes.Buffer{}
	w := &bytes.Buffer{}

	r.WriteString("test")

	if err := s.CopySingle(r, w); err != nil {
		t.Errorf("s.CopySingle: %v", err)
	}
	if err := s.CopySingle(r, w); err != nil {
		t.Errorf("s.CopySingle: %v", err)
	}
	{
		got := r.String()
		want := "st"
		if got != want {
			t.Errorf("Read buffer not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
	{
		got := w.String()
		want := "te"
		if got != want {
			t.Errorf("Write buffer not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
	if err := s.CopySingle(r, w); err != nil {
		t.Errorf("s.CopySingle: %v", err)
	}
	if err := s.CopySingle(r, w); err != nil {
		t.Errorf("s.CopySingle: %v", err)
	}
	{
		got := r.String()
		want := ""
		if got != want {
			t.Errorf("Read buffer not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
	{
		got := w.String()
		want := "test"
		if got != want {
			t.Errorf("Write buffer not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestCopyUntil(t *testing.T) {
	cfg := config.NewTesting(t)

	s := stream.New(cfg)
	r := &bytes.Buffer{}
	w := &bytes.Buffer{}
	testCases := []struct {
		read    string
		runes   []rune
		wantR   string
		wantW   string
		wantErr error
	}{
		{
			read:    "A quick brown fox jumped over a lazy dog.",
			runes:   []rune{},
			wantR:   "",
			wantW:   "A quick brown fox jumped over a lazy dog.",
			wantErr: io.EOF,
		},
		{
			read:    "A quick brown fox jumped over a lazy dog.",
			runes:   []rune{'!'},
			wantR:   "",
			wantW:   "A quick brown fox jumped over a lazy dog.",
			wantErr: io.EOF,
		},
		{
			read:  "A quick brown fox jumped over a lazy dog.",
			runes: []rune{'b'},
			wantR: "rown fox jumped over a lazy dog.",
			wantW: "A quick ",
		},
		{
			read:  "A quick brown fox jumped over a lazy dog.",
			runes: []rune{'j', 'd', 'w'},
			wantR: "n fox jumped over a lazy dog.",
			wantW: "A quick bro",
		},
		// Escaping characters will NOT cause them to be ignored.
		{
			read:  "A quick brown `fox` jumped over a lazy dog.",
			runes: []rune{'`'},
			wantR: "fox` jumped over a lazy dog.",
			wantW: "A quick brown ",
		},
		{
			read:  "A quick brown \\`fox` jumped over a lazy dog.",
			runes: []rune{'`'},
			wantR: "fox` jumped over a lazy dog.",
			wantW: "A quick brown \\",
		},
		{
			read:  "A quick br\\own f\\ox jumped over a lazy dog.",
			runes: []rune{'o'},
			wantR: "wn f\\ox jumped over a lazy dog.",
			wantW: "A quick br\\",
		},
		{
			read:  "A quick brown f\\ox jumped over a lazy dog.",
			runes: []rune{'o'},
			wantR: "wn f\\ox jumped over a lazy dog.",
			wantW: "A quick br",
		},
	}

	for _, tc := range testCases {
		r.Reset()
		w.Reset()

		r.WriteString(tc.read)

		_, err := s.CopyUntil(r, w, tc.runes...)
		if tc.wantErr != nil {
			if err == nil {
				t.Errorf("Expected an error but was nil!")
			} else {
				got := err.Error()
				want := tc.wantErr.Error()
				if got != want {
					t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
				}
			}
		} else {
			if err != nil {
				t.Errorf("s.CopyUntil: %v", err)
			}
		}
		{
			got := r.String()
			want := tc.wantR
			if got != want {
				t.Errorf("Read buffer not as expected: \n got: %q\nwant: %q", got, want)
			}
		}
		{
			got := w.String()
			want := tc.wantW
			if got != want {
				t.Errorf("Write buffer not as expected: \n got: %q\nwant: %q", got, want)
			}
		}
	}
}

func TestReadRune(t *testing.T) {
	cfg := config.NewTesting(t)

	s := stream.New(cfg)
	b := strings.NewReader("test")
	runes := []rune{'t', 'e', 's', 't'}

	for _, want := range runes {
		got, err := s.ReadRune(b)
		if err != nil {
			t.Errorf("s.ReadRune: %v", err)
		}
		if got != want {
			t.Errorf("Read rune not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestReadUntil(t *testing.T) {
	cfg := config.NewTesting(t)

	s := stream.New(cfg)
	r := &bytes.Buffer{}
	w := &bytes.Buffer{}
	testCases := []struct {
		read    string
		runes   []rune
		wantR   string
		wantW   string
		wantErr error
	}{
		{
			read:    "A quick brown fox jumped over a lazy dog.",
			runes:   []rune{},
			wantR:   "",
			wantW:   "A quick brown fox jumped over a lazy dog.",
			wantErr: io.EOF,
		},
		{
			read:    "A quick brown fox jumped over a lazy dog.",
			runes:   []rune{'!'},
			wantR:   "",
			wantW:   "A quick brown fox jumped over a lazy dog.",
			wantErr: io.EOF,
		},
		{
			read:  "A quick brown fox jumped over a lazy dog.",
			runes: []rune{'b'},
			wantR: "rown fox jumped over a lazy dog.",
			wantW: "A quick ",
		},
		{
			read:  "A quick brown fox jumped over a lazy dog.",
			runes: []rune{'j', 'd', 'w'},
			wantR: "n fox jumped over a lazy dog.",
			wantW: "A quick bro",
		},
		// Escaping characters will cause them to be ignored.
		{
			read:  "A quick brown `fox` jumped over a lazy dog.",
			runes: []rune{'`'},
			wantR: "fox` jumped over a lazy dog.",
			wantW: "A quick brown ",
		},
		{
			read:  "A quick brown \\`fox` jumped over a lazy dog.",
			runes: []rune{'`'},
			wantR: " jumped over a lazy dog.",
			wantW: "A quick brown \\`fox",
		},
		{
			read:  "A quick br\\own f\\ox jumped over a lazy dog.",
			runes: []rune{'o'},
			wantR: "ver a lazy dog.",
			wantW: "A quick br\\own f\\ox jumped ",
		},
		{
			read:  "A quick brown f\\ox jumped over a lazy dog.",
			runes: []rune{'o'},
			wantR: "wn f\\ox jumped over a lazy dog.",
			wantW: "A quick br",
		},
	}

	for _, tc := range testCases {
		r.Reset()
		w.Reset()

		r.WriteString(tc.read)

		_, err := s.ReadUntil(r, w, tc.runes...)
		if tc.wantErr != nil {
			if err == nil {
				t.Errorf("Expected an error but was nil!")
			} else {
				got := err.Error()
				want := tc.wantErr.Error()
				if got != want {
					t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
				}
			}
		} else {
			if err != nil {
				t.Errorf("s.ReadUntil: %v", err)
			}
		}
		{
			got := r.String()
			want := tc.wantR
			if got != want {
				t.Errorf("Read buffer not as expected: \n got: %q\nwant: %q", got, want)
			}
		}
		{
			got := w.String()
			want := tc.wantW
			if got != want {
				t.Errorf("Write buffer not as expected: \n got: %q\nwant: %q", got, want)
			}
		}
	}
}

func TestUnescape(t *testing.T) {
	cfg := config.NewTesting(t)

	s := stream.New(cfg)
	testCases := []struct {
		read    string
		want    string
		wantErr error
	}{
		{
			read:    "A quick brown fox jumped over a lazy dog.",
			want:    "A quick brown fox jumped over a lazy dog.",
			wantErr: io.EOF,
		},
		{
			read:    "A quick brown fox\\njumped over a lazy dog.",
			want:    "A quick brown fox\njumped over a lazy dog.",
			wantErr: io.EOF,
		},
		{
			read:    "A quick brown\\\\fox\\n\\njumped over a lazy dog.",
			want:    "A quick brown\\fox\n\njumped over a lazy dog.",
			wantErr: io.EOF,
		},
		{
			read:    "A quick brown fox \\!jumped over a lazy dog.",
			want:    "A quick brown fox ",
			wantErr: fmt.Errorf("Unknown escape sequence: \\!"),
		},
	}

	for _, tc := range testCases {
		r := strings.NewReader(tc.read)
		b := &bytes.Buffer{}

		err := s.Unescape(r, b)
		if tc.wantErr != nil {
			if err == nil {
				t.Errorf("Expected an error but was nil!")
			} else {
				got := err.Error()
				want := tc.wantErr.Error()
				if got != want {
					t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
				}
			}
		} else {
			if err != nil {
				t.Errorf("s.Unescape: %v", err)
			}
		}

		got := b.String()
		want := tc.want
		if got != want {
			t.Errorf("Write buffer not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestWriteRune(t *testing.T) {
	cfg := config.NewTesting(t)

	s := stream.New(cfg)
	b := &bytes.Buffer{}
	runes := []rune{'t', 'e', 's', 't'}

	for _, r := range runes {
		if err := s.WriteRune(b, r); err != nil {
			t.Errorf("s.WriteRune: %v", err)
		}
	}

	got := b.String()
	want := "test"
	if got != want {
		t.Errorf("Written byte buffer not as expected: \n got: %q\nwant: %q", got, want)
	}
}
