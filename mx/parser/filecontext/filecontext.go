package filecontext

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
)

type FileContext struct {
	Dir string
}

func (fc *FileContext) ChDir(dir string) error {
	rp := fc.RealPath(dir)

	d, err := os.Stat(rp)
	if err != nil {
		return err
	}
	if !d.IsDir() {
		return fmt.Errorf("Not a directory: %q", rp)
	}

	fc.Dir = fc.TryAbs(rp)
	return nil
}

func (fc *FileContext) ChFile(path string) error {
	rp := fc.RealPath(path)
	if err := fc.ChDir(filepath.Dir(rp)); err != nil {
		return err
	}
	return nil
}

func (fc *FileContext) FileName(p string) string {
	return path.Base(p)
}

func (fc *FileContext) RealPath(p string) string {
	if strings.HasPrefix(p, "/") {
		return p
	}

	p = filepath.Join(fc.Dir, p)
	if strings.HasPrefix(p, "./") {
		// Paths that start with ./ are bound to the context they originate from.
		return fc.TryAbs(p)
	}

	return p
}

func (fc *FileContext) TryAbs(p string) string {
	n, err := filepath.Abs(p)
	if err != nil {
		return p
	}
	return n
}

func New() *FileContext {
	fc := &FileContext{}

	fc.ChDir(".")

	return fc
}
