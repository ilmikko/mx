package filecontext_test

import (
	"testing"

	"gitlab.com/ilmikko/mx/mx/parser/filecontext"
)

func TestRealPath(t *testing.T) {
	testCases := []struct {
		path string
		want string
	}{
		{
			path: "/tmp",
			want: "/tmp",
		},
		{
			path: ".",
			want: "/tmp",
		},
		{
			path: "tmp",
			want: "/tmp/tmp",
		},
		{
			path: "./tmp",
			want: "/tmp/tmp",
		},
		{
			path: "./tmp",
			want: "/tmp/tmp",
		},
		{
			path: "..",
			want: "/",
		},
		{
			path: "../tmp",
			want: "/tmp",
		},
	}

	fc := filecontext.New()
	if err := fc.ChDir("/tmp"); err != nil {
		t.Fatalf("ChDir: %v", err)
	}

	for _, tc := range testCases {
		got := fc.RealPath(tc.path)
		want := tc.want
		if got != want {
			t.Errorf("Path not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestRetainDirectory(t *testing.T) {
	fc := filecontext.New()

	if err := fc.ChDir("/"); err != nil {
		t.Errorf("ChDir: %v", err)
	}

	{
		got := fc.Dir
		want := "/"
		if got != want {
			t.Errorf("Directory not as expected: \n got: %q\nwant: %q", got, want)
		}
	}

	if err := fc.ChDir("./tmp"); err != nil {
		t.Errorf("ChDir: %v", err)
	}

	{
		got := fc.Dir
		want := "/tmp"
		if got != want {
			t.Errorf("Directory not as expected: \n got: %q\nwant: %q", got, want)
		}
	}

	if err := fc.ChDir("."); err != nil {
		t.Errorf("ChDir: %v", err)
	}

	{
		got := fc.Dir
		want := "/tmp"
		if got != want {
			t.Errorf("Directory not as expected: \n got: %q\nwant: %q", got, want)
		}
	}

	if err := fc.ChDir(".."); err != nil {
		t.Errorf("ChDir: %v", err)
	}

	{
		got := fc.Dir
		want := "/"
		if got != want {
			t.Errorf("Directory not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}
