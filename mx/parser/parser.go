package parser

import (
	"bytes"
	"fmt"
	"io"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx/parser/filecontext"
	"gitlab.com/ilmikko/mx/mx/parser/stream"
	"gitlab.com/ilmikko/mx/mx/parser/variables"
	"gitlab.com/ilmikko/mx/mx/runes"
	"gitlab.com/ilmikko/mx/types/rule"
)

type Parser struct {
	CFG *config.Config

	R runes.Reader
	W runes.Writer

	Fc        *filecontext.FileContext
	Stream    *stream.Stream
	Variables *variables.Variables
}

func (p *Parser) Clone() (*Parser, error) {
	np := New(p.CFG)

	np.Variables = p.Variables.Clone()

	if err := np.Fc.ChDir(p.Fc.Dir); err != nil {
		return nil, err
	}

	return np, nil
}

// Next bytes are a command and its arguments.
func (p *Parser) Command(c runes.Reader) (string, string, error) {
	ecmd := &bytes.Buffer{}
	args := &bytes.Buffer{}

	for {
		if _, err := p.Stream.ReadUntil(c, ecmd, p.CFG.Rules[rule.SEPARATOR]); err != nil {
			if err == io.EOF {
				break
			}
			return "", "", err
		}
		if err := p.Stream.Unescape(c, args); err != nil {
			if err == io.EOF {
				break
			}
			return "", "", err
		}
		break
	}

	cmd := &bytes.Buffer{}
	if err := p.Stream.Unescape(ecmd, cmd); err != nil {
		if err != io.EOF {
			return "", "", err
		}
	}

	return cmd.String(), args.String(), nil
}

// Next bytes read are a pipeline of macro commands.
func (p *Parser) PipeLine(r runes.Reader) ([]*bytes.Buffer, error) {
	cmds := []*bytes.Buffer{}

	for {
		cmd := &bytes.Buffer{}
		_, err := p.Stream.ReadUntil(r, cmd, p.CFG.Rules[rule.PIPE])
		{
			if cmd.Len() > 0 {
				cmds = append(cmds, cmd)
			}
		}
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
	}
	return cmds, nil
}

// Next bytes read are generic. Could be anything!
// Take care here to not touch anything that isn't our Macro rune.
// Everything else should be left intact.
func (p *Parser) Next() (*bytes.Buffer, error) {
	macro := &bytes.Buffer{}

	if _, err := p.Stream.CopyUntil(p.R, p.W, p.CFG.Rules[rule.MACRO]); err != nil {
		return nil, err
	}
	if _, err := p.Stream.ReadUntil(p.R, macro, p.CFG.Rules[rule.MACRO]); err != nil {
		return nil, err
	}

	return macro, nil
}

func (p *Parser) SwapReadWrite() error {
	r := p.R
	br, ok := r.(*bytes.Buffer)
	if !ok {
		return fmt.Errorf("Cannot swap streams; Read stream is not *bytes.Buffer")
	}
	p.W = br
	return nil
}

func (p *Parser) Swap() error {
	r, w := p.R, p.W

	br, ok := r.(*bytes.Buffer)
	if !ok {
		return fmt.Errorf("Cannot swap streams; Read stream is not *bytes.Buffer")
	}

	bw, ok := w.(*bytes.Buffer)
	if !ok {
		return fmt.Errorf("Cannot swap streams; Write stream is not *bytes.Buffer")
	}

	p.R, p.W = bw, br
	return nil
}

func New(cfg *config.Config) *Parser {
	p := &Parser{}

	p.R = &bytes.Buffer{}
	p.W = &bytes.Buffer{}

	p.CFG = cfg
	p.Fc = filecontext.New()
	p.Stream = stream.New(cfg)
	p.Variables = variables.New()

	return p
}
