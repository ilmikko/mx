package config

import (
	"os"
	"strings"

	"gitlab.com/ilmikko/mx/types/command"
	"gitlab.com/ilmikko/mx/types/rule"
)

type Strings []string

func (s Strings) String() string {
	return strings.Join(s, "\n")
}

func (s *Strings) Set(v string) error {
	*s = append(*s, v)
	return nil
}

var (
	flagActions         Strings
	flagFiles           Strings
	flagTrailingNewline bool
)

func (c *Config) actionFile(f string) string {
	return command.INCLUDE.String() + rule.SEPARATOR.String() + f
}

func (c *Config) parseFlags() error {
	c.Flag.Var(&flagActions, "do", "Additional lines to parse. Precedes any files.")
	c.Flag.Var(&flagFiles, "file", "Files to parse. Shorthand for -do 'INCLUDE <file>'.")
	c.Flag.BoolVar(&flagTrailingNewline, "n", false, "Add a trailing newline after all actions have completed.")
	c.Flag.Parse(os.Args[1:])

	c.TrailingNewline = flagTrailingNewline

	c.Actions = flagActions
	for _, file := range flagFiles {
		c.Actions = append(c.Actions, c.actionFile(file))
	}

	// Try to figure out files/do from c.Flag.Args() for convenience.
	args := c.Flag.Args()
	for len(args) > 0 {
		var arg string
		arg, args = args[0], args[1:]

		// If the file exists, pass it as a -file.
		if s, _ := os.Stat(arg); s != nil {
			arg = c.actionFile(arg)
		}

		c.Actions = append(c.Actions, arg)
	}

	return nil
}
