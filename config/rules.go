package config

import (
	"fmt"

	"gitlab.com/ilmikko/mx/types/rule"
)

func (c *Config) GetRule(r rune) (rule.Rule, error) {
	for i, sr := range c.Rules {
		if r == sr {
			return i, nil
		}
	}
	return -1, fmt.Errorf("No rule assigned for rune %q", r)
}

func (c *Config) IsSpecialRune(r rune) bool {
	for _, sr := range c.Rules {
		if r == sr {
			return true
		}
	}
	return false
}
