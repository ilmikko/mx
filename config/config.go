package config

import (
	"flag"

	"gitlab.com/ilmikko/mx/types/command"
	"gitlab.com/ilmikko/mx/types/rule"
)

type Config struct {
	Actions         []string
	Commands        map[string]command.Command
	Rules           map[rule.Rule]rune
	Flag            *flag.FlagSet
	TrailingNewline bool
}

func Create() (*Config, error) {
	c := New()
	if err := c.parseFlags(); err != nil {
		return nil, err
	}
	return c, nil
}

func (c *Config) setDefaultCommands() {
	for i, s := range command.DEFAULT {
		c.Commands[s] = i
	}
}

func (c *Config) setDefaultRules() {
	for i, s := range rule.DEFAULT {
		c.Rules[i] = s
	}
}

func New() *Config {
	c := &Config{}

	c.Commands = map[string]command.Command{}
	c.Rules = map[rule.Rule]rune{}
	c.Flag = flag.NewFlagSet("MX", flag.ExitOnError)

	c.setDefaultCommands()
	c.setDefaultRules()

	return c
}
