package main

import (
	"bufio"
	"log"
	"os"

	"gitlab.com/ilmikko/mx/config"
	"gitlab.com/ilmikko/mx/mx"
)

func mainErr() error {
	cfg, err := config.Create()
	if err != nil {
		return err
	}

	m, err := mx.New(cfg)
	if err != nil {
		return err
	}

	o := bufio.NewWriter(os.Stdout)
	defer o.Flush()

	if err := m.ProcessStdin(o); err != nil {
		return err
	}

	if err := m.ProcessActions(cfg.Actions, o); err != nil {
		return err
	}

	if cfg.TrailingNewline {
		o.Write([]byte("\n"))
	}
	return nil
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("Error: %v", err)
	}
}
