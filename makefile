COVERFILE=coverage.out
GITPATH=gitlab.com/ilmikko/mx
PKGS=$(shell find . -type f -name "*.go" | xargs dirname | sort -u)

build:
	go build -o bin/mx main/main.go

coverage-gen:
	go test $(PKGS) -coverprofile=$(COVERFILE)

coverage-view:
	go tool cover -html=$(COVERFILE)

coverage: coverage-gen coverage-view

test-golden:
	cd golden && ./golden.sh

test-unit:
	go test $(PKGS)

test: test-unit build test-golden
