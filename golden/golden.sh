#!/usr/bin/env bash
# Golden tests for Toolbox.

for dir in ./*; do
	[ -d "$dir" ] || continue;
	[ -f "$dir/TEST" ] || continue;
	echo "$dir";
	(
	cd "$dir";
	mkdir -p "got";
	(
	cd "got";
	. "../TEST" || exit;
	);

	# Remove the timestamps from STDERR.
	if [ -f "got/STDERR" ]; then
		cat got/STDERR | sed -e 's/^[0-9]*\/[0-9]*\/[0-9]* [0-9]*:[0-9]*:[0-9]* //' > STDERR
		mv STDERR got/STDERR;
	fi

	diff --recursive "got" "want" || exit;
	rm -rf "got";
	) || exit;
done

echo OK
