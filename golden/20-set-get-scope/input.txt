You can set variables.
`ECHO VALUE|SET VARIABLE|ECHO 
The scope is inside quotes.
Value is \`GET VARIABLE\`.
\`ECHO 
Even on multiple lines,
value still remains \\\`GET VARIABLE\\\`.
\|EVAL\`
|EVAL`
