Wrap is a command that adds some text on both sides of a stream.
You could use it to `ECHO put things in parentheses|WRAP ( )`.
You can also wrap a specific side: `ECHO left side|WRAP.LEFT --> --> -->`
Or: `ECHO right side|WRAP.RIGHT <-- <-- <--`.
