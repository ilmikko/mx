package command

const (
	DIALECT Command = iota
	ECHO
	EVAL
	GET
	INCLUDE
	OVERRIDE
	PWD
	READ
	REMOVE
	SET
	TRIM
	TRIM_LEFT
	TRIM_RIGHT
	WRAP
	WRAP_LEFT
	WRAP_RIGHT
	WRITE
)

var (
	DEFAULT = map[Command]string{
		DIALECT:    "DIALECT",
		ECHO:       "ECHO",
		EVAL:       "EVAL",
		GET:        "GET",
		INCLUDE:    "INCLUDE",
		OVERRIDE:   "OVERRIDE",
		PWD:        "PWD",
		READ:       "READ",
		REMOVE:     "REMOVE",
		SET:        "SET",
		TRIM:       "TRIM",
		TRIM_LEFT:  "TRIM.LEFT",
		TRIM_RIGHT: "TRIM.RIGHT",
		WRAP:       "WRAP",
		WRAP_LEFT:  "WRAP.LEFT",
		WRAP_RIGHT: "WRAP.RIGHT",
		WRITE:      "WRITE",
	}
)

type Command int

func (c Command) String() string {
	s, _ := DEFAULT[c]
	return s
}
