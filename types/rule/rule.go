package rule

type Rule int

func (r Rule) Rune() rune {
	s, _ := DEFAULT[r]
	return s
}

func (r Rule) String() string {
	return string(r.Rune())
}

var (
	DEFAULT = map[Rule]rune{
		ESCAPE:    '\\', // Rune escape rune.
		MACRO:     '`',  // Macro from data separator.
		PIPE:      '|',  // Macro command output piping separator.
		SEPARATOR: ' ',  // Command argument separator.
	}
)

const (
	ESCAPE Rule = iota
	MACRO
	PIPE
	SEPARATOR
)
